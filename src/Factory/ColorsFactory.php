<?php

namespace App\Factory;

use App\Entity\Colors;
use App\Repository\ColorsRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Colors>
 *
 * @method        Colors|Proxy create(array|callable $attributes = [])
 * @method static Colors|Proxy createOne(array $attributes = [])
 * @method static Colors|Proxy find(object|array|mixed $criteria)
 * @method static Colors|Proxy findOrCreate(array $attributes)
 * @method static Colors|Proxy first(string $sortedField = 'id')
 * @method static Colors|Proxy last(string $sortedField = 'id')
 * @method static Colors|Proxy random(array $attributes = [])
 * @method static Colors|Proxy randomOrCreate(array $attributes = [])
 * @method static ColorsRepository|RepositoryProxy repository()
 * @method static Colors[]|Proxy[] all()
 * @method static Colors[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Colors[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Colors[]|Proxy[] findBy(array $attributes)
 * @method static Colors[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Colors[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class ColorsFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'code' => self::faker()->text(7),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Colors $colors): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Colors::class;
    }
}

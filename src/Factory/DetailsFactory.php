<?php

namespace App\Factory;

use App\Entity\Details;
use App\Repository\DetailsRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Details>
 *
 * @method        Details|Proxy create(array|callable $attributes = [])
 * @method static Details|Proxy createOne(array $attributes = [])
 * @method static Details|Proxy find(object|array|mixed $criteria)
 * @method static Details|Proxy findOrCreate(array $attributes)
 * @method static Details|Proxy first(string $sortedField = 'id')
 * @method static Details|Proxy last(string $sortedField = 'id')
 * @method static Details|Proxy random(array $attributes = [])
 * @method static Details|Proxy randomOrCreate(array $attributes = [])
 * @method static DetailsRepository|RepositoryProxy repository()
 * @method static Details[]|Proxy[] all()
 * @method static Details[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Details[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Details[]|Proxy[] findBy(array $attributes)
 * @method static Details[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Details[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class DetailsFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'quantity' => self::faker()->randomNumber(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Details $details): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Details::class;
    }
}

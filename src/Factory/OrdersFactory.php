<?php

namespace App\Factory;

use App\Entity\Orders;
use App\Repository\OrdersRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Orders>
 *
 * @method        Orders|Proxy create(array|callable $attributes = [])
 * @method static Orders|Proxy createOne(array $attributes = [])
 * @method static Orders|Proxy find(object|array|mixed $criteria)
 * @method static Orders|Proxy findOrCreate(array $attributes)
 * @method static Orders|Proxy first(string $sortedField = 'id')
 * @method static Orders|Proxy last(string $sortedField = 'id')
 * @method static Orders|Proxy random(array $attributes = [])
 * @method static Orders|Proxy randomOrCreate(array $attributes = [])
 * @method static OrdersRepository|RepositoryProxy repository()
 * @method static Orders[]|Proxy[] all()
 * @method static Orders[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Orders[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Orders[]|Proxy[] findBy(array $attributes)
 * @method static Orders[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Orders[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class OrdersFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'user' => UserFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Orders $orders): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Orders::class;
    }
}

<?php

namespace App\Factory;

use App\Entity\ColorProduct;
use App\Repository\ColorProductRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ColorProduct>
 *
 * @method        ColorProduct|Proxy create(array|callable $attributes = [])
 * @method static ColorProduct|Proxy createOne(array $attributes = [])
 * @method static ColorProduct|Proxy find(object|array|mixed $criteria)
 * @method static ColorProduct|Proxy findOrCreate(array $attributes)
 * @method static ColorProduct|Proxy first(string $sortedField = 'id')
 * @method static ColorProduct|Proxy last(string $sortedField = 'id')
 * @method static ColorProduct|Proxy random(array $attributes = [])
 * @method static ColorProduct|Proxy randomOrCreate(array $attributes = [])
 * @method static ColorProductRepository|RepositoryProxy repository()
 * @method static ColorProduct[]|Proxy[] all()
 * @method static ColorProduct[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ColorProduct[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ColorProduct[]|Proxy[] findBy(array $attributes)
 * @method static ColorProduct[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ColorProduct[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class ColorProductFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(ColorProduct $colorProduct): void {})
        ;
    }

    protected static function getClass(): string
    {
        return ColorProduct::class;
    }
}

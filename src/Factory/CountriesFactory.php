<?php

namespace App\Factory;

use App\Entity\Countries;
use App\Repository\CountriesRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Countries>
 *
 * @method        Countries|Proxy create(array|callable $attributes = [])
 * @method static Countries|Proxy createOne(array $attributes = [])
 * @method static Countries|Proxy find(object|array|mixed $criteria)
 * @method static Countries|Proxy findOrCreate(array $attributes)
 * @method static Countries|Proxy first(string $sortedField = 'id')
 * @method static Countries|Proxy last(string $sortedField = 'id')
 * @method static Countries|Proxy random(array $attributes = [])
 * @method static Countries|Proxy randomOrCreate(array $attributes = [])
 * @method static CountriesRepository|RepositoryProxy repository()
 * @method static Countries[]|Proxy[] all()
 * @method static Countries[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Countries[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Countries[]|Proxy[] findBy(array $attributes)
 * @method static Countries[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Countries[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class CountriesFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'createdAt' => self::faker()->dateTime(),
            'name' => self::faker()->text(255),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Countries $countries): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Countries::class;
    }
}

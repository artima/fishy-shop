<?php

namespace App\Factory;

use App\Entity\Addresses;
use App\Repository\AddressesRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Addresses>
 *
 * @method        Addresses|Proxy create(array|callable $attributes = [])
 * @method static Addresses|Proxy createOne(array $attributes = [])
 * @method static Addresses|Proxy find(object|array|mixed $criteria)
 * @method static Addresses|Proxy findOrCreate(array $attributes)
 * @method static Addresses|Proxy first(string $sortedField = 'id')
 * @method static Addresses|Proxy last(string $sortedField = 'id')
 * @method static Addresses|Proxy random(array $attributes = [])
 * @method static Addresses|Proxy randomOrCreate(array $attributes = [])
 * @method static AddressesRepository|RepositoryProxy repository()
 * @method static Addresses[]|Proxy[] all()
 * @method static Addresses[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Addresses[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Addresses[]|Proxy[] findBy(array $attributes)
 * @method static Addresses[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Addresses[]|Proxy[] randomSet(int $number, array $attributes = [])
 */
final class AddressesFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'btwNumber' => self::faker()->text(255),
            'city' => self::faker()->text(100),
            'country' => CountriesFactory::new(),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'houseNumber' => self::faker()->text(5),
            'street' => self::faker()->text(255),
            'type' => self::faker()->text(20),
            'user' => UserFactory::new(),
            'zipcode' => self::faker()->text(10),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Addresses $addresses): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Addresses::class;
    }
}

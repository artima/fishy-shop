<?php

namespace App\Controller;

use App\Entity\ColorProduct;
use App\Form\ColorProductType;
use App\Repository\ColorProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/color/product')]
class ColorProductController extends AbstractController
{
    #[Route('/', name: 'app_color_product_index', methods: ['GET'])]
    public function index(ColorProductRepository $colorProductRepository): Response
    {
        return $this->render('color_product/index.html.twig', [
            'color_products' => $colorProductRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_color_product_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ColorProductRepository $colorProductRepository): Response
    {
        $colorProduct = new ColorProduct();
        $form = $this->createForm(ColorProductType::class, $colorProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colorProductRepository->save($colorProduct, true);

            return $this->redirectToRoute('app_color_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('color_product/new.html.twig', [
            'color_product' => $colorProduct,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_color_product_show', methods: ['GET'])]
    public function show(ColorProduct $colorProduct): Response
    {
        return $this->render('color_product/show.html.twig', [
            'color_product' => $colorProduct,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_color_product_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ColorProduct $colorProduct, ColorProductRepository $colorProductRepository): Response
    {
        $form = $this->createForm(ColorProductType::class, $colorProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colorProductRepository->save($colorProduct, true);

            return $this->redirectToRoute('app_color_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('color_product/edit.html.twig', [
            'color_product' => $colorProduct,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_color_product_delete', methods: ['POST'])]
    public function delete(Request $request, ColorProduct $colorProduct, ColorProductRepository $colorProductRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$colorProduct->getId(), $request->request->get('_token'))) {
            $colorProductRepository->remove($colorProduct, true);
        }

        return $this->redirectToRoute('app_color_product_index', [], Response::HTTP_SEE_OTHER);
    }
}

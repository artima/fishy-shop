<?php

namespace App\Controller;

use App\Entity\Colors;
use App\Form\ColorsType;
use App\Repository\ColorsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/colors')]
class ColorsController extends AbstractController
{
    #[Route('/', name: 'app_colors_index', methods: ['GET'])]
    public function index(ColorsRepository $colorsRepository): Response
    {
        return $this->render('colors/index.html.twig', [
            'colors' => $colorsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_colors_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ColorsRepository $colorsRepository): Response
    {
        $color = new Colors();
        $form = $this->createForm(ColorsType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colorsRepository->save($color, true);

            return $this->redirectToRoute('app_colors_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('colors/new.html.twig', [
            'color' => $color,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_colors_show', methods: ['GET'])]
    public function show(Colors $color): Response
    {
        return $this->render('colors/show.html.twig', [
            'color' => $color,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_colors_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Colors $color, ColorsRepository $colorsRepository): Response
    {
        $form = $this->createForm(ColorsType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $colorsRepository->save($color, true);

            return $this->redirectToRoute('app_colors_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('colors/edit.html.twig', [
            'color' => $color,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_colors_delete', methods: ['POST'])]
    public function delete(Request $request, Colors $color, ColorsRepository $colorsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$color->getId(), $request->request->get('_token'))) {
            $colorsRepository->remove($color, true);
        }

        return $this->redirectToRoute('app_colors_index', [], Response::HTTP_SEE_OTHER);
    }
}

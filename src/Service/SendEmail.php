<?php

// src/Service/ImageHandler.php
namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class SendEmail
{
    private $mailer;
    private $senderEmail;
    private $senderName;

    public function __construct(MailerInterface $mi, string $senderEmail, string $senderName)
    {
        $this->mailer = $mi;
        $this->senderEmail = $senderEmail;
        $this->senderName = $senderName;
    }

    
    public function send(array $arguments): void
    {
        [
            'recipient_email'   => $recipientemail,
            'subject'           => $subject,
            'html_template'     => $htmltemplate,
            'context'           => $context,
        ] = $arguments;
        $email = (new TemplatedEmail());
        $email->from('hello@example.com')
            ->to($recipientemail)
            ->subject($subject)
            ->htmlTemplate($htmltemplate)
            ->context($context);

        try{
        $this->mailer->send($email);
        //dd($this->mailer);
        } catch(TransportExceptionInterface $mailerException) {
            throw $mailerException;
        }

    }
}

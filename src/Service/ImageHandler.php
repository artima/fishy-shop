<?php

// src/Service/ImageHandler.php
namespace App\Service;

use App\Entity\Photo;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImageHandler
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function managePhoto($entity, $form)
    {
        if ($photos = $form->get("photos")->getData()) {
            // On boucle sur les images
            foreach ($photos as $photo) {
                $nomFichier = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
                $slugger = new AsciiSlugger();
                $nomFichier = $slugger->slug($nomFichier);
                $nomFichier .= "_" . uniqid();
                $nomFichier .= "." . $photo->guessExtension();
                $photo->move(
                    $this->params->get('images_directory'),
                    $nomFichier
                );
                
                if($photo = $entity->getPhotos()->contains($photo)) {
                    $fichier = $this->params->get("image_directory") . $photo()->getName();
                    if (file_exists($fichier)) {
                        unlink($fichier);
                    }
                }

                $img = new Photo();
                $img->setName($nomFichier);
                $entity->addPhoto($img);
            }
        }
    }
}
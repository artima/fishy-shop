<?php

namespace App\DataFixtures;

use App\Factory\UserFactory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        UserFactory::createOne(['email' => 'mit.iza@example.com', 'password' => 'mitiza', 'roles' => ['ROLE_ADMIN']]);
        UserFactory::new()->createMany(20);

        $manager->flush();
    }
}